package com.apartmentseller.announcementservice.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AnnouncementDTO {

    private long id;
    private String text;
    private long user_id;
    private String author;
    private LocalDateTime creationTime;
    private String filename;

}
