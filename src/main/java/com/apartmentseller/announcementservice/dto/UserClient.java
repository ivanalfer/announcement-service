package com.apartmentseller.announcementservice.dto;

import lombok.Data;

@Data
public class UserClient {

    private String username;
    private long id;

}
