package com.apartmentseller.announcementservice.service;

import com.apartmentseller.announcementservice.dto.AnnouncementDTO;
import com.apartmentseller.announcementservice.dto.GetAnnouncementResponseDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface AnnouncementService {

    List<GetAnnouncementResponseDTO> getAllAnnouncement();
    AnnouncementDTO addAnnouncement(AnnouncementDTO announcementDto, MultipartFile file);
    void deleteAnnouncement(long announcementId/*, UserDto currentUser*/);
    Optional<GetAnnouncementResponseDTO> getAnnouncement(long announcementId);
    AnnouncementDTO updateAnnouncement(long announcementId, AnnouncementDTO announcement/*, UserDto userDto*/);
}
