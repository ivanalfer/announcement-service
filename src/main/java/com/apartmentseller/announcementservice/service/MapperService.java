package com.apartmentseller.announcementservice.service;

import com.apartmentseller.announcementservice.domain.Announcement;
import com.apartmentseller.announcementservice.dto.AnnouncementDTO;
import com.apartmentseller.announcementservice.dto.GetAnnouncementResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MapperService {

    MapperService INSTANCE = Mappers.getMapper(MapperService.class);

    AnnouncementDTO announcementEntityMapToAnnouncementDto(Announcement announcement);
    GetAnnouncementResponseDTO announcementEntityMapToGetAnnouncementResponseDTO(Announcement announcement);

    Announcement announcementDtoMapToAnnouncementEntity(AnnouncementDTO announcementDto);
}
