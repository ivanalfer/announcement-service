package com.apartmentseller.announcementservice.service.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AnnouncementNotFoundException extends RuntimeException {
}
