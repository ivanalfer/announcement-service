package com.apartmentseller.announcementservice.repository;

import com.apartmentseller.announcementservice.domain.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {
}
