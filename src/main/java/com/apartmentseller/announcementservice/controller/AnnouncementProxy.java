package com.apartmentseller.announcementservice.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "apartment-seller")
public interface AnnouncementProxy {

    @GetMapping("/announcement")
    Map<Long, String> getAuthorsOfTheAnnouncements(@RequestParam("usersId") List<Long> usersId);

    @GetMapping("/user/{userId}/announcement")
    String getAuthorOfTheAnnouncement(@PathVariable(value = "userId") Long userId);
}
