package com.apartmentseller.announcementservice.controller;

import com.apartmentseller.announcementservice.dto.GetAnnouncementResponseDTO;
import com.apartmentseller.announcementservice.service.AnnouncementService;
import com.apartmentseller.announcementservice.service.exception.AnnouncementNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class AnnouncementController {

    private final AnnouncementService announcementService;
    private final AnnouncementProxy announcementProxy;

    @Autowired
    public AnnouncementController(AnnouncementService announcementService, AnnouncementProxy announcementProxy) {
        this.announcementService = announcementService;
        this.announcementProxy = announcementProxy;
    }


    @GetMapping
    public List<GetAnnouncementResponseDTO> getAllAnnouncement() {
        List<GetAnnouncementResponseDTO> allAnnouncement = announcementService.getAllAnnouncement();
        Map<Long, String> authors = announcementProxy.getAuthorsOfTheAnnouncements(allAnnouncement.stream()
                .map(GetAnnouncementResponseDTO::getUser_id).collect(Collectors.toList())
        );
        allAnnouncement.forEach(announcement -> announcement.setAuthor(
                authors.get(announcement.getUser_id())
        ));
        return allAnnouncement;
    }

    @GetMapping("{id}")
    public GetAnnouncementResponseDTO getAnnouncement(@PathVariable("id") long announcementId) {
        GetAnnouncementResponseDTO announcementDTO = announcementService.getAnnouncement(announcementId)
                .orElseThrow(AnnouncementNotFoundException::new);
        announcementDTO.setAuthor(announcementProxy.getAuthorOfTheAnnouncement(
                announcementDTO.getUser_id())
        );
        return announcementDTO;
    }
//
//    @PostMapping
//    public AnnouncementDTO addAnnouncement(@RequestPart AnnouncementDTO announcement,
////                                           @AuthenticationPrincipal UserDto userDto,
//                                           @RequestParam(required = false, value = "file") MultipartFile file) {
////        announcement.setAuthor(userDto);
//        return announcementService.addAnnouncement(announcement, file);
//    }
//
//    @PutMapping("{id}")
//    public AnnouncementDTO updateAnnouncement(@PathVariable("id") long announcementId,
//                                              @RequestBody AnnouncementDTO announcement/*,
//                                              @AuthenticationPrincipal UserDto userDto*/) {
//        return announcementService.updateAnnouncement(announcementId, announcement/*, userDto*/);
//    }
//
//    @DeleteMapping("{id}")
//    public void deleteAnnouncement(@PathVariable("id") long announcementId/*, @AuthenticationPrincipal UserDto currentUser*/) {
//        announcementService.deleteAnnouncement(announcementId/*, currentUser*/);
//    }

}
